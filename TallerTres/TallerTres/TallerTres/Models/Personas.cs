﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace TallerTres.Models
{
    class Personas
    {
        [PrimaryKey, AutoIncrement, Column("_id")]
        public int Id { get; set; }

        [MaxLength(150)]

        public string Name { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Sexo { get; set; }

    }
}
