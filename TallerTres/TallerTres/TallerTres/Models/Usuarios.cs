﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace TallerTres.Models
{
    class Usuarios
    {
        [PrimaryKey, AutoIncrement, Column("_id")]
        public int Id { get; set; }

        [MaxLength(150)]

        public string Name_User{ get; set; }
        public string Password { get; set; }
        public string avatar { get; set; }
        public Boolean estado { get; set; }
    }
}
