﻿using TallerTres.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TallerTres
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ListarUsuarios : ContentPage
    {
        public ListarUsuarios()
        {
            InitializeComponent();
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();

            using (SQLite.SQLiteConnection connection = new SQLite.SQLiteConnection(App.urlBd))
            {
                List<Usuarios> listaUsua;
                listaUsua = connection.Table<Usuarios>().ToList();
                listView_Us.ItemsSource = listaUsua;
            }
        }
    }
}

