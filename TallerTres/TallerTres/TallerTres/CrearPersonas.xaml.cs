﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TallerTres.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TallerTres
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CrearPersonas : ContentPage
	{
		public CrearPersonas ()
		{
			InitializeComponent ();
		}

       
        async private void guardar(object sender, EventArgs e)
        {
            int selectedIndex = pikerGenero.SelectedIndex;

            
            if (string.IsNullOrEmpty(entryName.Text)  || string.IsNullOrEmpty(entryPhone.Text) || 
                string.IsNullOrEmpty(entryEmail.Text) || (selectedIndex==-1))

            {
                
                labelMessage2.TextColor = Color.Red;
                labelMessage2.Text = "Debe Completar Todos los Datos.";

                await DisplayAlert("Error", "Debe Completar los Campos.", "Ok");
            }
            else
            {
                
                labelMessage2.TextColor = Color.Green;
                labelMessage2.Text = "Sus Datos Han Sido Guardados Correctamente";

                List_Pers1();

                entryName.Text = "";
                entryPhone.Text = "";
                entryEmail.Text = "";
                pikerGenero.SelectedIndex = -1;
                await Navigation.PushAsync(new ListarPersonas());

            }
        }
        

        public void List_Pers1()
         {
            int selectedIndex = pikerGenero.SelectedIndex;

            Personas personas = new Personas()
            {
                Name = entryName.Text,
                Phone = entryPhone.Text,
                Email = entryEmail.Text,
                Sexo = pikerGenero.Items[selectedIndex]
            };
            using (SQLite.SQLiteConnection connection = new SQLite.SQLiteConnection(App.urlBd))
            {
                //Crear una tabla en base de datos  
                connection.CreateTable<Personas>();

                //Crear un registro en la tabla "Tarea"
                var result = connection.Insert(personas);

                if (result > 0)
                {
                    DisplayAlert("Correcto", "Registro Guradado", "Ok");
                    
                }
                else
                {
                    DisplayAlert("Error", "No Se Guardo Registro", "Ok");
                }
            };
        }


        async private void List_Pers()
        {

            await Navigation.PushAsync(new ListarPersonas());
        }



    }
}