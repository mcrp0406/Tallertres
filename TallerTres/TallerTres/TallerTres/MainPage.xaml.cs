﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace TallerTres
{
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
		}

        async private void ing_Person (object sender, EventArgs e)
        {
           
                await DisplayAlert("Correcto", "Ingreso Sesion de Persona", "Ok");

                await Navigation.PushAsync(new CrearPersonas());
            
        }
        async private void ing_User (object sender, EventArgs e)
        {

            await DisplayAlert("Correcto", "Ingreso Sesion de Usuario", "Ok");

            await Navigation.PushAsync(new CrearUsuarios());

        }

    }
}
