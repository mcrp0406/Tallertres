﻿using TallerTres.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TallerTres
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CrearUsuarios : ContentPage
	{
		public CrearUsuarios ()
		{
			InitializeComponent ();
		}
        async private void guardar(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(entryName_user.Text) || string.IsNullOrEmpty(entryPassword.Text) ||
                string.IsNullOrEmpty(entryAvatar.Text))
            {
                labelMessage.TextColor = Color.Red;
                labelMessage.Text = "Debe Completar Todos los Datos.";

                await DisplayAlert("Error", "Debe Completar los Campos.", "Ok");
            }
            else
            {
                
                labelMessage.TextColor = Color.Green;
                labelMessage.Text = "Sus Datos Han Sido Guardados Correctamente";

                Lis_User1();

                entryName_user.Text = "";
                entryPassword.Text = "";
                entryAvatar.Text = "";
                await Navigation.PushAsync(new ListarUsuarios());
            }
        }


        public void Lis_User1()
        {
            
            Usuarios usuarios = new Usuarios()
            {
                Name_User = entryName_user.Text,
                Password = entryPassword.Text,
                avatar = entryAvatar.Text,
                estado = true
            };
            using (SQLite.SQLiteConnection connection = new SQLite.SQLiteConnection(App.urlBd))
            {
                connection.CreateTable<Usuarios>();
                var result = connection.Insert(usuarios);

                if (result > 0)
                {
                    DisplayAlert("Correcto", "Usuario Guradado", "Ok");

                }
                else
                {
                    DisplayAlert("Error", "No Se Guardo Usuario", "Ok");
                }
            };
        }

        async private void Lis_User()
        {
            await Navigation.PushAsync(new ListarUsuarios());
        }


    }
}